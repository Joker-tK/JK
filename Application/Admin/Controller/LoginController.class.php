<?php
namespace Admin\Controller;
use Think\Controller;
/**
 * 登陆控制器
 */
 class LoginController extends Controller{
 	/**
	 * 默认方法
	 */
	public function index(){
		if(IS_POST){
			$m = D('Admin/User');
			if(!$m->aLogin()){
				$this->error($m->error);
				echo 123;
			}
			//$this->success('登陆成功',U('Index/index'));
			
		}
		$this->display('login');
	}
	/**
	 * 验证码
	 */
	public function verify(){
		$Verify = new \Think\Verify();
		$Verify->entry();
	}	
	/**
	 * 退出
	 */
	public function out(){
		unset($_SESSION['aid']);
		unset($_SESSION['aemail']);
		$this->success('退出成功',U('Login/index'));
	}
	
 }
